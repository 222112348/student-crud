/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.nanoyulian.studentdemocrudwebapp.service;

import com.nanoyulian.studentdemocrudwebapp.dto.StudentDto;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author samue
 */
@Service
public interface StudentService {
    
    public List<StudentDto> ambilDaftarStudent();
    public void perbaruiDataStudent (StudentDto studentDto);
    public void hapusDataStudent (Long studentId);
    public void simpanDataStudent (StudentDto studentDto);
    public StudentDto cariById(Long id);
}
