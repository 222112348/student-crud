/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.nanoyulian.studentdemocrudwebapp.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.nanoyulian.studentdemocrudwebapp.entity.Student;

/**
 *
 * @author samue
 */
public interface StudentRepository extends JpaRepository< Student , Long>{
        Optional<Student> findByLastName (String LastName);
}
