/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nanoyulian.studentdemocrudwebapp.controller;


import com.nanoyulian.studentdemocrudwebapp.dto.StudentDto;
import com.nanoyulian.studentdemocrudwebapp.service.StudentService;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author samue
 */
@Controller

public class StudentController {
    private StudentService studentService;
    
    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }
    
    //handler method, GET Request return model (dto) dan View (templates/*.html)
    @GetMapping("/admin/students")
    public String students(Model model){
        List<StudentDto> studentDtos = this.studentService.ambilDaftarStudent();
        //tambah atribut "students" yang bisa/akan digunakan di view
        model.addAttribute("studentDtos", studentDtos);
        // thymeleaf view: "/templates/admin/student.html"
        return "/admin/students";
    }
    
    @GetMapping("/admin/students/add")
    public String addStudentForm(Model model){
        StudentDto studentDto = new StudentDto();
        // tambah atribut "studentDto" yang bisa/akan digunakan di form th:object
        model.addAttribute("studentDto",studentDto);
        // thymeleaf view: "/templates/admin/students.html"
        return "/admin/student_add_form";
    }
    
    
    //handler method untuk request view index
    @GetMapping("/")
    public String index () {
        return "index";
    }

    @PostMapping("/admin/students/add")
    public String addStudent(@Valid StudentDto studentDto, BindingResult result){
        if (result.hasErrors()){
            // model.addAttribute("studentDto", studentDto);
            return "/admin/student_add_form";
        }
        studentService.simpanDataStudent(studentDto);
        return "redirect:/admin/students";
    }
    @GetMapping("/admin/students/delete/{id}")
    public String deleteStudent(@PathVariable("id") Long id){
        studentService.hapusDataStudent(id);
        return "redirect:/admin/students";
    }
    @GetMapping("/admin/students/update/{id}")
    public String updateStudentForm(@PathVariable("id") Long id, Model model){
        StudentDto studentDto = studentService.cariById(id);
        model.addAttribute("studentDto", studentDto);
        return "/admin/student_update_form";
    }


    @PostMapping("/admin/students/update")
    public String updateStudent(@Valid @ModelAttribute("studentDto") StudentDto studentDto, BindingResult result){
        if (result.hasErrors()){
            // model.addAttribute("studentDto", studentDto);
            return "/admin/student_update_form";
        }
        studentService.perbaruiDataStudent(studentDto);
        return "redirect:/admin/students";
    }
}
